require 'api_constraints'

Rails.application.routes.draw do
  devise_for :users
  
  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
      resources :users, :only => [:show, :create, :update, :destroy] do
        resources :articles, :only => [:index, :show, :create, :update, :destroy]
      end
      resources :sessions, :only => [:create, :destroy]
      get "/articles",      to: "articles#index_public"
      get "/articles/:id",  to: "articles#show_public"
    end

    scope module: :v2, constraints: ApiConstraints.new(version: 2) do
      match('/sessions(/:id)', to: "sessions#init", via: :post)
      match('/users(/:id)', to: "users#init", via: :post)
      match('/articles(/:id)', to: "articles#init", via: :post)
    end
  end
  
end
