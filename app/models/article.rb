class Article < ActiveRecord::Base
	belongs_to :user

	validates :title, :user_id, :category, :content, :published, presence: true

	self.per_page = 10

	scope :published, lambda { where(published: true) }
	scope :newest_first, lambda { order(created_at: :desc) }
  scope :title_search, lambda { |title| where( ["title LIKE ?", "%#{title}%"]) }
  scope :category_search, lambda { |category| where(["category = ?", category]) }
end
