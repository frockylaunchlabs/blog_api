class User < ActiveRecord::Base
	validates :auth_token, uniqueness: true
	
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_uploader :avatar, AvatarUploader
  
	before_create :generate_authentication_token!

	has_many :articles, dependent: :destroy

  self.per_page = 10

  def generate_authentication_token!
    begin
      self.auth_token = Devise.friendly_token
    end while self.class.exists?( auth_token: auth_token)
  end

end
