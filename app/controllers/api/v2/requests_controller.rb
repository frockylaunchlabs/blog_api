USERS_CTRLR = Api::V2::UsersController
ARTICLES_CTRLR = Api::V2::ArticlesController
SESSIONS_CTRLR = Api::V2::SessionsController

class Api::V2::RequestsController < ApplicationController

	def process_request
		result = "not working"
		ctrlr = nil

		case params[:ctrlr]
			when 'sessions' 
				ctrlr = SESSIONS_CTRLR.init() 

			when 'users'
				ctrlr = USERS_CTRLR.init( params) 
				if result = ctrlr.run_method
					render json: {
						jsonrpc: "2.0",
						result: result.as_json(only: [:id, :email, :auth_token, :avatar]),
						id: ctrlr.id
					}
				else
					render json: {
						jsonrpc: "2.0",
						error: ctrlr.error_message,
						id: ctrlr.id
					}
				end

			when 'articles'
				ctrlr = ARTICLES_CTRLR.init( params )
				if result = ctrlr.run_method
					render json: {
						jsonrpc: "2.0",
						result: result.as_json(only: [:title, :category, :content, :published, :user_id]),
						id: ctrlr.id
					}
				else
					render json: {
						jsonrpc: "2.0",
						error: ctrlr.error_message,
						id: ctrlr.id
					}
				end

			else
				render json: {
						jsonrpc: "2.0",
						error: "Controller #{params[:ctrlr]} not found!",
						id: params[:id]
					}

		end

	end

end
