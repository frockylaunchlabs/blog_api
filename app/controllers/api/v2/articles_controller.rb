class Api::V2::ArticlesController < ApplicationController
  before_action :authenticate_with_token!, except: [:index_public, :show_public, :init]
  before_action :page, only: [:index, :index_public]

  def init
    json_responce = {}
    json_responce[:jsonrpc] = "2.0"
    status = 200

    case params[:method]
      when 'index_public'
        json_responce[:result] = index_public 

      when 'show_public'
        json_responce[:result] = show_public

      when 'index'
        json_responce[:result] = index

      when 'show'
        json_responce[:result]= show

      when 'create'
        json_responce[:result], json_responce[:id]  = create

      when 'update'
        json_responce[:result] = update

      when 'destroy'
        json_responce[:result], status = destroy

      else
        raise
    end
    
    unless @error_message
      render json: json_responce, status: status
    else
      json_responce.delete(:result)
      json_responce[:error] = @error_message
      render json: json_responce
    end

  end

  def index
    if user = current_user
      articles = user.articles.paginate( page: page)
    else
      @error_message = "Not authenticated!"
      false
    end
  end

  def show
    if user = current_user
      if article = user.articles.find_by( id: params[:id])
        article
      else
        @error_message = "Article not found!"
        false
      end
    else
      @error_message = "Not authenticated!"
      false
    end
  end

  def index_public
    articles = Article.order( created_at: :desc)
    articles = articles.search( search_params) if search_params.present?
    articles.paginate( page: page)
  end

  def show_public
    if article = Article.find_by( id: params[:id])
      article
    else
      @error_message = "Article not found!"
      false
    end
  end

  def create
    if user = current_user
      article = user.articles.build(article_params)
      if article.save
        [article, article.id]
      else
        @error_message = article.errors.full_messages
        false
      end
    else
      @error_message = "Unable to create, not authenticated!"
      false
    end
  end

  def update
    if user = current_user
      if article = user.articles.find_by( id: params[:id])
        if article.update(article_params)
          article
        else
          @error_message = article.errors.full_messages
          false
        end
      else
        @error_message = "Unable to edit, article not found!"
        false
      end
    else
      @error_message = "Unable to edit, not authenticated!"
      false
    end
  end

  def destroy
    if user = current_user
      if article = user.articles.find_by( id: params[:id])
        article.destroy
        [article, 204]
      else
        @error_message = "Unable to delete, article not found!"
        false
      end
    else
      @error_message = "Unable to delete, not authenticated!"
      false
    end
  end

  private

    def article_params
      params.require(:params)
            .permit(:title, :category, :content, :published)
    end

    def search_params
      params.require(:search)
            .permit(:title, :category) if params[:search].present?
    end

    def page
      params[:params][:page].present? ?
        params[:params][:page] : 1
    end

    def format_json(object=nil)
      {jsonrpc: "2.0",
        result: object.as_json(only: [:id, :title, :category, :content]) ,
        error: nil,
        id: nil }
      
    end

end
