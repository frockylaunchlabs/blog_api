class Api::V2::SessionsController < ApplicationController

  def init
    case params[:method]
	    when 'create'
	    	create
	    when 'destroy'
	    	destroy
	    else
	    	raise
	    end
  end

  def create 
    user_password = params[:params][:password]
    user_email = params[:params][:email]
    if user = user_email.present? && User.find_by(email: user_email)
	    if user.valid_password? user_password
	      sign_in user, store: false
	      user.generate_authentication_token!
	      user.save
	      render json: user, only: [:id,:email,:auth_token]
	    else
	      render json: { errors: "Wrong password!" }
	    end
	  else
	  	render json: { errors: "Invalid email!"}
	  end
  end

  def destroy
    user = User.find_by( id: params[:id])    
    user.generate_authentication_token!
    user.save
    head 204 # :no_content
  end

end