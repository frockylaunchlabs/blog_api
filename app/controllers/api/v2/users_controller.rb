class Api::V2::UsersController < ApplicationController
  before_action :authenticate_with_token!, except: [:create]

  def init
      case params[:method]
        when 'show'
          show
        when 'create'
          create
        when 'update'
          update
        when 'destroy'
          destroy
        else
          raise
      end
  end

  def show
    json_responce = {}
    if user = User.find_by(id: params[:id])
      json_responce[:jsonrpc] = "2.0"
      json_responce[:result]  = user
      json_responce[:id] = params[:id] if params[:id]
      render json: json_responce
    else
      json_responce[:jsonrpc] = "2.0"
      json_responce[:error] = { message: "User not found!"}
      json_responce[:id] = params[:id] if params[:id]
      render json: json_responce
    end
  end

  def create
    json_responce = {}
    user = User.new(user_params)
    if user.save
      json_responce[:jsonrpc] = "2.0"
      json_responce[:result]  = user
      json_responce[:id] = params[:id] if params[:id]
      render json: json_responce
    else
      json_responce[:jsonrpc] = "2.0"
      json_responce[:error] = { message: user.errors.full_messages}
      json_responce[:id] = params[:id] if params[:id]
      render json: json_responce
    end
  end

  def update
    json_responce = {}
    user = current_user
    if user.update(user_params)
      json_responce[:jsonrpc] = "2.0"
      json_responce[:result]  = user
      json_responce[:id] = params[:id] if params[:id]
      render json: json_responce
    else
      json_responce[:jsonrpc] = "2.0"
      json_responce[:error] = { message: user.errors.full_messages}
      json_responce[:id] = params[:id] if params[:id]
      render json: json_responce
    end
  end

  def destroy
    user = current_user
    user.destroy
    head 204
  end

  private

    def user_params
      params.require(:params).permit(:email, :password, :password_confirmation, :avatar) 
    end

end
