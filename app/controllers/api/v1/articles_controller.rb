class Api::V1::ArticlesController < ApplicationController
  before_action :authenticate_with_token!, except: [:index_public, :show_public]
  before_action :page, only: [:index, :index_public]

	def index
    articles = current_user.articles.paginate( page: page)
		responder articles
	end

	def show
		article = current_user.articles.find_by( id: params[:id])
		responder article
	end

  def index_public
    articles = Article.published.newest_first
    articles = articles.category_search(params[:category]) if params[:category].present?
    articles = articles.title_search(params[:search]) if params[:search].present?
    articles = articles.paginate( page: page)
    responder articles, [:title, :category, :content]
  end

  def show_public
    article = Article.published.find_by( id: params[:id])
    responder article, [:title, :category, :content]
  end

	def create
    article = current_user.articles.build(article_params)
    if article.save
      @id = article.id
      @error_message = false
    else
      @error_message = article.errors.full_messages
    end
    responder article
  end

  def update
  	article = current_user.articles.find_by( id: params[:id])
  	if article.update(article_params)
      @error_message = false
  	else
      @error_message = article.errors.full_messages
  	end
    responder article
  end

  def destroy
  	article = current_user.articles.find_by( id: params[:id])
  	article.destroy
    head 204
  end

  # def responder(object=nil, options=[])
  #   response = {}
  #   response[:jsonrpc] = "2.0"
  #   unless @error_message
  #     response[:result]  = object
  #     response[:result]  = object.as_json(only: options) if options.present?
  #     response[:id] = params[:id] || @id
  #   else
  #     response[:error] = {message: @error_message}
  #     response[:id] = params[:id] || @id
  #   end
  #   render json: response
  # end

	private

    def article_params
      params.require(:article)
            .permit(:title, :category, :content, :published)
    end

    def page
      params[:page].present? ?
        params[:page] : 1
    end

end
