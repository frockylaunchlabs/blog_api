class Api::V1::SessionsController < ApplicationController

  def create 
    user_password = params[:session][:password]
    user_email = params[:session][:email]
    user = user_email.present? && User.find_by(email: user_email) 

    if user.valid_password? user_password
      sign_in user, store: false
      user.generate_authentication_token!
      user.save
      @id = user.id
      responder user, [:email,:auth_token]
    else
      @error_message = "Invalid email/password!"
    end 
  end

  def destroy
    user = User.find_by( id: params[:id])    
    user.generate_authentication_token!
    user.save
    head 204 # :no_content
  end

end