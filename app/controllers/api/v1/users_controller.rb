class Api::V1::UsersController < ApplicationController
  before_action :authenticate_with_token!, except: :create

  def show
    if user = User.find_by_id( params[:id])
      @id = user.id
      @error_message = false
    else
      @error_message = "User not found!"
      false
    end
    responder user, [:email, :auth_token, :avatar]
  end

  def create
    user = User.new( user_params)
    if user.save
      @id = user.id
      @error_message = false
    else
      @error_message = user.errors.full_messages
    end
    responder user, [:email, :auth_token, :avatar]
  end

  def update
    if current_user.update(user_params)
      @error_message = false
    else
      @error_message = current_user.errors.full_messages
    end
    responder current_user, [:email, :auth_token, :avatar]
  end

  def destroy
    current_user.destroy
    head 204
  end

  # def responder(object=nil, options=[])
  #   response = {}
  #   response[:jsonrpc] = "2.0"
  #   unless @error_message
  #     response[:result]  = object.as_json(only: options)
  #     response[:id] = params[:id] || @id
  #   else
  #     response[:error] = {message: @error_message}
  #     response[:id] = params[:id] || @id
  #   end
  #   render json: response
  # end

  private

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :avatar) 
    end

end
