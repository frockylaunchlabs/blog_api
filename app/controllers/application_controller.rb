class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token

  # Not the ideal place for helper codes
  def current_user
    # a || a = b,  If a is false, nil or undefined, then evaluate b and set a to the result.
    current_user ||= User.find_by( auth_token: request.headers['Authorization'] )
  end

  def authenticate_with_token!
    response = {}
    response[:jsonrpc] = "2.0"
    response[:error] = {message: "Not authenticated!"}
    response[:id] = params[:id] if params[:id]
    render json: response, status: :unauthorized unless user_signed_in?
  end

  def user_signed_in?
    current_user.present? 
  end

  def responder(object=nil, options=[])
    response = {}
    response[:jsonrpc] = "2.0"
    unless @error_message
      response[:result]  = object
      response[:result]  = object.as_json(only: options) if options.present?
      response[:id] = params[:id] || @id
    else
      response[:error] = {message: @error_message}
      response[:id] = params[:id] || @id
    end
    render json: response
  end

end
